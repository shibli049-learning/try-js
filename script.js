var date = new Date();
document.body.innerHTML = "<h1>Today is: " + date + "</h1>";
document.body.innerHTML += "<h2>hello world from external js!!</h2>";

var negInteger = -3.14159265359;
var escQuote = "Quotes can also be \"escaped\".";
var theSunIsWarm = true;
var emptyInside = null;
var justAnotherVariable;

// Try this in your console:
console.log(typeof insertVariableName);
console.log(typeof justAnotherVariable);
console.log(typeof emptyInside);
console.log(typeof theSunIsWarm);
console.log(typeof escQuote);
console.log(typeof negInteger);


//one number and another is string number
{
    var a = 4;
    var b = "5";

    console.log(a+b);
    console.log(a-b);
    console.log(a*b);
    console.log(a/b);
}

//one number and another is string words
{
    var a = 4;
    var b = "hello";

    console.log(a+b);
    console.log(a-b);
    console.log(a*b);
    console.log(a/b);
}