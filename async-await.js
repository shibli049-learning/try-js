if(false){
    const delay = (seconds) => {
        return new Promise(
            resolve => setTimeout(resolve, seconds * 1000)
        )
    };
    
    const countToFive = async() => {
        var msg = () => console.log("hello");
        console.log("zero seconds: " + new Date());
        await delay(1).then(msg);
        console.log("one seconds: " + new Date());
        await delay(1).then(msg);    
        console.log("two seconds: " + new Date());
        await delay(3).then(msg);
        console.log("five seconds: " + new Date());
    };
    
    countToFive();
    
}


const githubRequest = async(loginName) => {
    try {
        var response = await fetch(`https://api.github.com/users/${loginName}/followers`);
        var json = await response.json();
        var followers = json.map(user => user.login);
        console.log(followers.join(", "));
    } catch (error) {
        console.error("Failed to load data", error); 
    }
}


githubRequest("shibli049");