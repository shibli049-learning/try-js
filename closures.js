function giveMeEms(pixels){
    var baseValue = 16;

    function doTheMath(){
        return pixels / baseValue;
    }

    return doTheMath;
}