const HOUR_HAND = document.querySelector("#hour");
const MINUTE_HAND = document.querySelector("#minute");
const SECOND_HAND = document.querySelector("#second");

var date = new Date();
let hr = date.getHours();
let min = date.getMinutes();
let sec = date.getSeconds();
console.log(`hours:${hr}, minutes:${min}, seconds:${sec}`);


var secPosition = sec * 6;
var minPosition = (min * 6) + (secPosition/60);
var hrPosition = (hr * 30) + (minPosition/12);

function moveTheHandsSmooth(){
    


    secPosition = secPosition + 6;
    minPosition = minPosition + (6/60);
    hrPosition = hrPosition + (3/360);

    HOUR_HAND.style.transform = `rotate(${hrPosition}deg)`;
    MINUTE_HAND.style.transform = `rotate(${minPosition}deg)`;
    SECOND_HAND.style.transform = `rotate(${secPosition}deg)`;

}

function moveTheHands(){
    
    var date = new Date();
    let hr = date.getHours();
    let min = date.getMinutes();
    let sec = date.getSeconds();
    console.log(`hours:${hr}, minutes:${min}, seconds:${sec}`);


    secPosition = sec * 6;
    minPosition = (min * 6) + (secPosition/60);
    hrPosition = (hr * 30) + (minPosition/12);

    
    HOUR_HAND.style.transform = `rotate(${hrPosition}deg)`;
    MINUTE_HAND.style.transform = `rotate(${minPosition}deg)`;
    SECOND_HAND.style.transform = `rotate(${secPosition}deg)`;

}


var interval = setInterval(moveTheHands, 1000);