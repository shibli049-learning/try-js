var myVar = 5;

console.log("before, myVar:", myVar);

function logScope(){ 
    var myVar = 2;
    if(myVar) {
        let myVar = "I'm different!!";
        console.log("nested myVar: ", myVar);
    }

	console.log("logScope myVar: ", myVar);
}

logScope();
console.log("after, myVar:", myVar);