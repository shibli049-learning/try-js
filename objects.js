//long hand
//  var course = new Object();
//  course.title = "JS for beginners";
//  course.instructor = "shibli";
//  course.level = 1;
//  course.views = 0;

// short hand

var course = {
    title: "JS for beginners",
    instructor: "shibli",
    level: 1,
    views: 0,
    updateViews: function(){
        return ++this.views;
    }
}

console.log(course.updateViews());
console.log(course.updateViews());
console.log(course.updateViews());
console.log(course);

function Course(title, instructor, level, views = 0){
    this.title = title;
    this.instructor = instructor;
    this.level = level;
    this.views = views;
    this.updateViews = function(){
        return ++this.views;
    }
}

var c2 = new Course("JS advanced", "shibli", 2);

console.log(c2);
c2.updateViews();
c2.updateViews();
console.log(c2);