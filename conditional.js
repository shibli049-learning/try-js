{
    var a = 5;
    var b = 6;
    var theNumbersMatch

    if ( a == b ) {
        theNumbersMatch = true;
    } else {
        theNumbersMatch = false;
    }

    console.log(theNumbersMatch);
}



// compare string with number
{
    var a = 5;
    var b = "5";
    var theNumbersMatch

    if ( a == b ) {
        theNumbersMatch = true;
    } else {
        theNumbersMatch = false;
    }

    console.log(theNumbersMatch);
}

// to compare in strict mode, use === instead of ==
{
    var a = 5;
    var b = "5";
    var theNumbersMatch

    if ( a === b ) {
        theNumbersMatch = true;
    } else {
        theNumbersMatch = false;
    }

    console.log(theNumbersMatch);
}